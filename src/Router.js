import React from "react-native";
import { createDrawerNavigator, createStackNavigator } from "react-navigation";
import Home from "./Home";
import Menu from "./Menu";
import DetailScreen from "./components/DetailScreen";

export const RootStack = createStackNavigator(
  { Home, DetailScreen },
  { navigationOptions: { header: null } }
);
export default (RootDrawer = createDrawerNavigator(
  {
    RootStack
  },
  {
    contentComponent: Menu
  }
));
