import React, { Component } from "react";
import { View, Text } from "react-native";
import MenuItem from "./components/MenuItem";
import {
  MenuItem1,
  MenuItem2,
  MenuItem3,
  MenuItem4,
  MenuItem5
} from "../fixture/Constaint";
class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menus: [
        { title: MenuItem1, nameIcon: "ios-home" },
        { title: MenuItem2, nameIcon: "ios-filing" },
        { title: MenuItem3, nameIcon: "ios-paper" },
        { title: MenuItem4, nameIcon: "ios-bookmark" },
        { title: MenuItem5, nameIcon: "ios-create" }
      ]
    };
  }

  render() {
    const { container } = styles;
    const { menus } = this.state;
    return <View style={container}>
        {menus.map((menu, index) => (
          <MenuItem title={menu.title} nameIcon={menu.nameIcon} key={index} />
        ))}
      </View>;
  }
}

const styles = {
  container: {
    padding: 15
  }
};

export default Menu;
