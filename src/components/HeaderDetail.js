import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

class HeaderDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const { container, textTitle } = styles;
        const { title, onPressButtonLeft } = this.props;
        return (
            <View style={container}>
                <TouchableOpacity onPress={onPressButtonLeft}>
                    <Icon name={"ios-arrow-back"} color={"#000"} size={25} />
                </TouchableOpacity>
                <View>
                    <Text style={textTitle}>{title}</Text>
                </View>
                <TouchableOpacity>
                    <Icon name={"ios-more"} color={"#000"} size={25} />
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = {
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        height: 50,
        backgroundColor: "#5166c0",
        paddingHorizontal: 15
    },
    textTitle: {
        color: "#fff",
        fontSize: 20,
        fontWeight: "bold"
    }
};

export default HeaderDetail;
