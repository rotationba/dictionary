import React, { Component } from "react";
import { View, Text, WebView, Dimensions } from "react-native";
import HeaderDetail from "./HeaderDetail";

const { height } = Dimensions.get("window");
class DetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      word: ""
    };
  }
  componentWillReceiveProps(nextProps) {
    const word = nextProps.navigation.getParam("word");
    this.setState({ word });
  }
  componentWillMount() {
    const word = this.props.navigation.getParam("word");
    this.setState({ word });
  }

  render() {
    const { word } = this.state;
    const { goBack } = this.props.navigation;
    console.log(word);
    return (
      <View>
        <HeaderDetail title={word[1]} onPressButtonLeft={() => goBack()} />
        <View style={{ height: height - 50 }}>
          <WebView
            source={{ html: word[2] }}
            automaticallyAdjustContentInsets={false}
          />
        </View>
      </View>
    );
  }
}

export default DetailScreen;
