import React, { Component } from "react";
import { View, Text, TextInput } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: ""
    };
  }

  render() {
    const { container, input } = styles;
    const { search } = this.state;
    return (
      <View style={container}>
        <TextInput
          style={input}
          placeholder="Nhập vào từ để tra"
          value={search}
          onChangeText={search => {
            this.setState({ search });
          }}
        />
        <Icon name={"ios-mic"} color={"#000"} size={25} />
      </View>
    );
  }
}

const styles = {
  container: {
    flexDirection: "row",
    height: 50,
    borderBottomColor: "#000",
    borderBottomWidth: 1,
    // backgroundColor: "red",
    alignItems: "center",
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  input: {
    alignSelf: "stretch"
  }
};

export default Search;
