import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";

class Word extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { input, output, onPress } = this.props;
    const { container, txtInput } = styles;
    return (
      <TouchableOpacity style={container} onPress={onPress}>
        <Text style={txtInput}>{input}</Text>
        <Text>{output}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = {
  container: {
    backgroundColor: "#fff",
    borderRadius: 5,
    paddingHorizontal: 5,
    paddingVertical: 10,
    marginBottom: 10,
  },
  txtInput: {
      fontSize: 19,
      fontWeight: 'bold',
  }
};

export default Word;
