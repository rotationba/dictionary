import React, { Component } from "react";
import { View, Text, FlatList, RefreshControl, ActivityIndicator } from "react-native";
import Header from "./components/Header";
import { NAME_TITLE } from "../fixture/Constaint";
import Search from "./components/SearchComponent";
import Word from "./components/WordComponent";

const data = require("../data/anh-viet.json");
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      fetching: false,
      words: [],
      limit: 10,
      page: 1
    };
  }
  componentDidMount() {
    this.getData();
  }
  getData = () => {
    const { limit, page } = this.state;
    let dataInit = data.slice();
    dataInit.length = limit * page;
    this.setState({ words: dataInit });
  };
  handleRefresh = refreshing => {
    if (refreshing) {
      this.setState({ refreshing: true, page: 1 }, async () => {
        await this.getData();
        this.setState({ refreshing: false });
      });
    }
  };
  handleLoadMore = () => {
    this.setState({ fetching: true, page: this.state.page + 1 }, () => {
      this.getData();
    });
  };
  onOpenMenu = () => {
    this.props.navigation.openDrawer();
  };
  handleToDetailScreen = (word) => {
    const { navigate } = this.props.navigation;
    navigate('DetailScreen', { word })
  }
  render() {
    const { container, flatList, loader } = styles;
    const { words, refreshing, fetching } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: "#c1c1c1" }}>
        <Header title={NAME_TITLE} onPressButtonLeft={this.onOpenMenu} />
        <View style={container}>
          <Search />
          <FlatList
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={() => this.handleRefresh("refreshing")}
              />
            }
            style={flatList}
            data={words}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => (
              <Word
                onPress={() => this.handleToDetailScreen(item)}
                input={item[1]}
                output={item[2].split("<li>")[1].split("<")[0]}
              />
            )}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0.1}
            ListFooterComponent={
              fetching && (
                <View style={loader}>
                  <ActivityIndicator size="large" color="#0000ff" />
                </View>
              )
            }
          />
        </View>
      </View>
    );
  }
}

const styles = {
  container: {
    padding: 5
  },
  flatList: {
    marginBottom: 110
  },
  loader: {
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    paddingHorizontal: 20
  }
};

export default Home;
